import "./App.css";
import MovieItem from "./components/MovieItem";

function App() {
  const movie = {
    title: "Alien",
    released: "22 Jun 1979",
    director: "Ridley Scott",
    poster: "https://i.imgur.com/tKbaVEK.jpeg",
  };

  return (
    <div className="App">
      <MovieItem {...movie} />
    </div>
  );
}

export default App;
